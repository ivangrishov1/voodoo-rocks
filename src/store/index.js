import { createStore } from 'vuex'
import axios from "axios";

const store = createStore({
    state () {
        return {
            posts: [],
            users: [],
            newPosts: [],
            searchQuery: '',
        }
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts
        },
        setUsers(state, users) {
            state.users = users
        },
        setSearchQuery(state, searchQuery) {
            state.searchQuery = searchQuery
        },
        setData(state) {
            state.posts.forEach((item,index) => {
                let itemPost = {}
                itemPost.title = item.title;
                itemPost.body = item.body;
                itemPost.author = state.users[index].name;
                state.newPosts.push(itemPost);

            })
        },
    },
    actions: {
        getAllData({commit}) {
            function getPosts() {
                return axios.get('http://jsonplaceholder.typicode.com/posts', {
                    params: {
                        _limit: 10
                    }
                })
            }
            function getUsers() {
                return axios
                    .get('http://jsonplaceholder.typicode.com/users', {
                        params: {
                            _limit: 10
                        }
                    })
            }
            axios.all([getPosts(), getUsers()])
                .then(axios.spread(function (posts, users) {
                    commit('setPosts', posts.data)
                    commit('setUsers', users.data)
                    commit('setData')
                }))
        },
    },
    getters: {
        sortedPosts(state) {
            return state.newPosts.filter(post => post.author.toLowerCase().includes(state.searchQuery.toLowerCase()));
        },
    },
})

export default store
