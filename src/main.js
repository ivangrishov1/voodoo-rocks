import { createApp } from 'vue'
import App from './App.vue'
import store from "@/store";
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import MasonryWall from "@yeger/vue-masonry-wall";

let app = createApp(App)


app.use(store)
app.use(MasonryWall)
app.mount('#app')
